package com.production.sergeypishiy.testtask.animals;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AnimalsResponse implements Serializable {
    @SerializedName("data")
    public Animal[] animals;

    public class Animal implements Serializable {
        @SerializedName("url")
        public String imageUrl;

        @SerializedName("title")
        public String title;
    }
}
