package com.production.sergeypishiy.testtask.mvp;

import com.production.sergeypishiy.testtask.animals.AnimalsResponse;

public class AnimalsFragmentPresenter implements BaseAnimalsFragmentContract.Presenter {

    private BaseAnimalsFragmentContract.View animalsFragmentView;

    @Override
    public void viewCreated(BaseAnimalsFragmentContract.View mvpView) {
        animalsFragmentView = mvpView;
    }

    @Override
    public void animalClicked(AnimalsResponse.Animal animal, int position) {
        animalsFragmentView.showAnimalDetails(animal, position);
    }
}
