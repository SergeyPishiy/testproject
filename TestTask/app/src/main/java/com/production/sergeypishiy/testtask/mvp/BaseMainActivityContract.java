package com.production.sergeypishiy.testtask.mvp;

import com.production.sergeypishiy.testtask.animals.AnimalsProvider;
import com.production.sergeypishiy.testtask.animals.AnimalsResponse;

public interface BaseMainActivityContract {

    interface View {
        void showConnectionErrorDialog(Exception e);

        void showProgressBar();

        void hideProgressBar();

        void showTryAgain();

        void hideTryAgain();

        void showCatsFragment();

        void showDogsFragment();

        void setProviders(AnimalsProvider catsProvider, AnimalsProvider dogsProvider);

        void navigate(int tabPosition);

        void showAnimalDetails(AnimalsResponse.Animal animal, int position);
    }

    interface Presenter {

        void viewCreated(View mvpView);

        void viewStarted();

        void viewStopped();

        void navigated(int tabIndex);

        void tryAgainClicked();

        void animalClicked(AnimalsResponse.Animal animal, int position);
    }
}
