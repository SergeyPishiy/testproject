package com.production.sergeypishiy.testtask.mvp;

import com.production.sergeypishiy.testtask.animals.AnimalsProvider;
import com.production.sergeypishiy.testtask.animals.AnimalsResponse;

public class MainActivityPresenter implements BaseMainActivityContract.Presenter {

    private BaseMainActivityContract.View mainActivityView;

    private AnimalsProvider.AnimalsProviderTask catsTask;
    private AnimalsProvider catsProvider;

    private AnimalsProvider.AnimalsProviderTask dogsTask;
    private AnimalsProvider dogsProvider;

    private int tabPosition = 0;
    private boolean isConnectionError = false;



    @Override
    public void viewCreated(BaseMainActivityContract.View mvpView) {
        mainActivityView = mvpView;
        if(catsProvider != null && dogsProvider != null) {
            mainActivityView.setProviders(catsProvider, dogsProvider);
        }
        mainActivityView.navigate(tabPosition);
    }

    @Override
    public void viewStarted() {
        if(!isConnectionError) {
            if ((catsProvider == null || dogsProvider == null)) {
                loadProviders();
            }
        } else {
            mainActivityView.showTryAgain();
        }
    }

    @Override
    public void viewStopped() {
        if(catsTask != null && !catsTask.getTask().isCanceled() && !catsTask.getTask().isComplete()) {
            catsTask.cancelTask();
            catsProvider = null;
        }
        if(dogsTask != null && !dogsTask.getTask().isCanceled() && !dogsTask.getTask().isComplete()) {
            dogsTask.cancelTask();
            dogsProvider = null;
        }
    }

    @Override
    public void navigated(int tabIndex) {
        tabPosition = tabIndex;
        if(tabIndex == 0) {
            mainActivityView.showCatsFragment();
        } else {
            mainActivityView.showDogsFragment();
        }
    }

    @Override
    public void tryAgainClicked() {
        loadProviders();
    }

    @Override
    public void animalClicked(AnimalsResponse.Animal animal, int position) {

    }

    private void onConnectionError(Exception e) {
        isConnectionError = true;
        mainActivityView.hideProgressBar();
        mainActivityView.showTryAgain();
        mainActivityView.showConnectionErrorDialog(e);
    }

    private void loadProviders() {
        isConnectionError = false;
        mainActivityView.hideTryAgain();
        mainActivityView.showProgressBar();
        catsTask = AnimalsProvider.getCatsProvider();
        catsTask.getTask().addOnSuccessListener(cats -> {
            dogsTask = AnimalsProvider.getDogsProvider();
            dogsTask.getTask()
                    .addOnSuccessListener(dogs -> {
                        catsProvider = cats;
                        dogsProvider = dogs;
                        mainActivityView.setProviders(catsProvider, dogsProvider);
                        mainActivityView.showCatsFragment();
                        mainActivityView.hideProgressBar();
                    })
                    .addOnFailureListener(e -> {
                        onConnectionError(e);
                    });
        })
                .addOnFailureListener(e -> {
                    onConnectionError(e);
                });
    }
}
