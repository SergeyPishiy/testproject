package com.production.sergeypishiy.testtask.dagger;

import com.production.sergeypishiy.testtask.mvp.AnimalsFragmentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class AnimalsFragmentPresenterModule {

    @Provides
    AnimalsFragmentPresenter provideMainActivityPresenter() {
        return new AnimalsFragmentPresenter();
    }
}
