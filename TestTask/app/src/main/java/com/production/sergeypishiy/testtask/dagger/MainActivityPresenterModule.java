package com.production.sergeypishiy.testtask.dagger;

import android.content.Context;

import com.production.sergeypishiy.testtask.mvp.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityPresenterModule {

    @Provides
    @Singleton
    MainActivityPresenter provideMainActivityPresenter() {
        return new MainActivityPresenter();
    }
}
