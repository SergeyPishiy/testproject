package com.production.sergeypishiy.testtask.dagger;

import com.production.sergeypishiy.testtask.mvp.MainActivityPresenter;
import com.production.sergeypishiy.testtask.ui.AnimalsFragment;
import com.production.sergeypishiy.testtask.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {MainActivityPresenterModule.class, AnimalsFragmentPresenterModule.class})
@Singleton
public interface AppComponent {

    void injectMainActivity(MainActivity mainActivity);

    void injectAnimalsFragment(AnimalsFragment animalsFragment);
}
