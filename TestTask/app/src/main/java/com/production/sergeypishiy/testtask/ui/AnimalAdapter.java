package com.production.sergeypishiy.testtask.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.production.sergeypishiy.testtask.R;
import com.production.sergeypishiy.testtask.animals.AnimalsResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;


public class AnimalAdapter extends RecyclerView.Adapter<AnimalAdapter.AnimalViewHolder> {

    public interface AnimalActionListener {
        void onClick(int position);
    }

    private ArrayList<AnimalsResponse.Animal> animals;
    private AnimalActionListener animalActionListener;

    public AnimalAdapter(Context context, AnimalsResponse.Animal[] animals, AnimalActionListener animalActionListener) {
        this.animals = new ArrayList(Arrays.asList(animals));
        this.animalActionListener = animalActionListener;
    }


    @NonNull
    @Override
    public AnimalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View animalView = LayoutInflater.from(parent.getContext()).inflate(R.layout.animal_item, parent, false);
        return new AnimalViewHolder(animalView);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimalViewHolder holder, final int position) {
        AnimalsResponse.Animal animal = animals.get(position);
        holder.animalNumber.setText(Integer.toString(position+1));
        holder.animalInfo.setText(animal.title);
        Picasso.get()
            .load(animal.imageUrl)
            .into(holder.animalImage);
    }

    @Override
    public int getItemCount() {
        return animals.size();
    }

    public AnimalsResponse.Animal getItem(int position) {
        return animals.get(position);
    }

    public class AnimalViewHolder extends RecyclerView.ViewHolder {
        private TextView animalNumber;
        private TextView animalInfo;
        private ImageView animalImage;

        AnimalViewHolder(View itemView) {
            super(itemView);
            animalNumber = itemView.findViewById(R.id.tv_number);
            animalInfo = itemView.findViewById(R.id.tv_info);
            animalImage = itemView.findViewById(R.id.img_animal);
            itemView.setOnClickListener(v -> {
                animalActionListener.onClick(getAdapterPosition());
            });
        }
    }
}