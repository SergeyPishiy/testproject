package com.production.sergeypishiy.testtask.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.production.sergeypishiy.testtask.R;
import com.production.sergeypishiy.testtask.animals.AnimalsResponse;
import com.squareup.picasso.Picasso;

public class AnimalDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_details);

        AnimalsResponse.Animal animal = (AnimalsResponse.Animal)getIntent().getSerializableExtra("animal");
        if(animal != null) {
            Picasso.get()
                    .load(animal.imageUrl)
                    .into((ImageView)findViewById(R.id.img_animal_detailed));
            ((TextView)findViewById(R.id.tv_info_detailed)).setText(animal.title);
        }
        int number = getIntent().getIntExtra("number", -1);
        if(number >= 0) {
            ((TextView)findViewById(R.id.tv_number_detailed)).setText(Integer.toString(number));
        }
    }
}
