package com.production.sergeypishiy.testtask.animals;

import com.google.android.gms.tasks.CancellationToken;
import com.google.android.gms.tasks.CancellationTokenSource;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.production.sergeypishiy.testtask.animals.AnimalsResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnimalsProvider implements Serializable {
    private ArrayList<AnimalsResponse.Animal[]> animals = new ArrayList();

    protected AnimalsProvider(AnimalsResponse.Animal[] animals) {
        if(animals != null) {
            this.animals = new ArrayList(Arrays.asList(animals));
        }
    }

    public AnimalsResponse.Animal[] getAnimals() {
        return animals.toArray(new AnimalsResponse.Animal[animals.size()]);
    }

    public static AnimalsProviderTask getCatsProvider() {
        return getAnimalsProvider("cat");
    }

    public static AnimalsProviderTask getDogsProvider() {
        return getAnimalsProvider("dog");
    }

    private static AnimalsProviderTask getAnimalsProvider(String id) {
        CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        TaskCompletionSource<AnimalsProvider> completionSource  = new TaskCompletionSource(cancellationTokenSource.getToken());

        AnimalsApiInterface animalsApiInterface = AnimalsApiClient.getClient().create(AnimalsApiInterface.class);
        Call animalsCall = animalsApiInterface.getAnimals(id);
        animalsCall.enqueue(new Callback<AnimalsResponse>() {
            @Override
            public void onResponse(Call<AnimalsResponse> call, Response<AnimalsResponse> response) {
                if (response == null || response.body() == null || response.code() != 200) {
                    completionSource.setException(new Exception("Error response"));
                    return;
                }
                completionSource.setResult(new AnimalsProvider(response.body().animals));
            }

            @Override
            public void onFailure(Call<AnimalsResponse> call, Throwable t) {
                call.cancel();
                completionSource.setException(new Exception(t));
            }
        });

        completionSource.getTask().addOnCanceledListener(() -> {
            if(!animalsCall.isExecuted() && !animalsCall.isCanceled()) {
                animalsCall.cancel();
            }
        });

        return new AnimalsProviderTask(completionSource.getTask(), cancellationTokenSource);
    }

    public static class AnimalsProviderTask {
        private Task<AnimalsProvider> task;
        private CancellationTokenSource cancellationTokenSource;


        public AnimalsProviderTask(Task<AnimalsProvider> task, CancellationTokenSource cancellationTokenSource) {
            this.task = task;
            this.cancellationTokenSource = cancellationTokenSource;
        }

        public Task<AnimalsProvider> getTask() {
            return task;
        }

        public void cancelTask() {
            cancellationTokenSource.cancel();
        }
    }
}
