package com.production.sergeypishiy.testtask.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.production.sergeypishiy.testtask.App;
import com.production.sergeypishiy.testtask.R;
import com.production.sergeypishiy.testtask.animals.AnimalsProvider;
import com.production.sergeypishiy.testtask.animals.AnimalsResponse;
import com.production.sergeypishiy.testtask.mvp.BaseMainActivityContract;
import com.production.sergeypishiy.testtask.mvp.MainActivityPresenter;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements BaseMainActivityContract.View {

    private final String TAG = this.getClass().toString();
    private static final String CATS_FRAGMENT_TAG = "CATS_FRAGMENT";
    private static final String DOGS_FRAGMENT_TAG = "DOGS_FRAGMENT";

    private static final String IS_INITIALIZED_KEY = "IS_INITIALIZED";

    private AnimalsFragment catsFragment;

    private AnimalsFragment dogsFragment;

    private TabLayout tabLayout;
    private Button btnTryAgain;

    @Inject
    MainActivityPresenter presenter;

    private boolean isInitialized = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((App)getApplication()).getAppComponent().injectMainActivity(this);



        if(savedInstanceState != null) {
            isInitialized = savedInstanceState.getBoolean(IS_INITIALIZED_KEY, false);
            if(isInitialized) {
                catsFragment = (AnimalsFragment) getSupportFragmentManager().getFragment(savedInstanceState, CATS_FRAGMENT_TAG);
                dogsFragment = (AnimalsFragment) getSupportFragmentManager().getFragment(savedInstanceState, DOGS_FRAGMENT_TAG);
            } else {
                catsFragment = new AnimalsFragment();
                dogsFragment = new AnimalsFragment();
            }
        } else {
            catsFragment = new AnimalsFragment();
            dogsFragment = new AnimalsFragment();
        }

        btnTryAgain = findViewById(R.id.btn_try_again);
        btnTryAgain.setOnClickListener(v -> {
            presenter.tryAgainClicked();
        });

        tabLayout = findViewById(R.id.navigation);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                presenter.navigated(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        presenter.viewCreated(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(isInitialized && catsFragment != null && dogsFragment != null) {
            getSupportFragmentManager().putFragment(outState, CATS_FRAGMENT_TAG, catsFragment);
            getSupportFragmentManager().putFragment(outState, DOGS_FRAGMENT_TAG, dogsFragment);
            outState.putBoolean(IS_INITIALIZED_KEY, isInitialized);
        }
    }

    @Override
    public void navigate(int tabPosition) {
        tabLayout.getTabAt(tabPosition).select();
    }

    @Override
    public void showAnimalDetails(AnimalsResponse.Animal animal, int position) {
        Intent intent = new Intent(this, AnimalDetailsActivity.class);
        intent.putExtra("animal", animal);
        intent.putExtra("number", position + 1);
        startActivity(intent);
    }

    @Override
    public void setProviders(AnimalsProvider catsProvider, AnimalsProvider dogsProvider) {
        Bundle catsArguments = new Bundle();
        catsArguments.putSerializable(AnimalsFragment.ANIMAL_PROVIDER_KEY, catsProvider);
        catsFragment.setArguments(catsArguments);

        Bundle dogsArguments = new Bundle();
        dogsArguments.putSerializable(AnimalsFragment.ANIMAL_PROVIDER_KEY, dogsProvider);
        dogsFragment.setArguments(dogsArguments);
    }

    @Override
    public void showCatsFragment() {
        if(!isInitialized) {
            isInitialized = true;
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_content, dogsFragment, DOGS_FRAGMENT_TAG);
            fragmentTransaction.add(R.id.fragment_content, catsFragment, CATS_FRAGMENT_TAG);
            fragmentTransaction.attach(catsFragment);
            fragmentTransaction.commit();
        } else {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.detach(dogsFragment);
            fragmentTransaction.attach(catsFragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void showDogsFragment() {
        if(!isInitialized) {
            isInitialized = true;
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_content, dogsFragment, DOGS_FRAGMENT_TAG);
            fragmentTransaction.add(R.id.fragment_content, catsFragment, CATS_FRAGMENT_TAG);
            fragmentTransaction.attach(dogsFragment);
            fragmentTransaction.commit();
        } else {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.detach(catsFragment);
            fragmentTransaction.attach(dogsFragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void showConnectionErrorDialog(Exception e) {
        Toast.makeText(this, getString(R.string.dialog_connection_error), Toast.LENGTH_SHORT).show();
        e.printStackTrace();
    }

    @Override
    public void showProgressBar() {
        findViewById(R.id.progress_bar_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        findViewById(R.id.progress_bar_layout).setVisibility(View.GONE);
        findViewById(R.id.progress_bar).setVisibility(View.GONE);
    }

    @Override
    public void showTryAgain() {
        findViewById(R.id.btn_try_again).setVisibility(View.VISIBLE);
        findViewById(R.id.progress_bar_layout).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideTryAgain() {
        findViewById(R.id.btn_try_again).setVisibility(View.GONE);
        findViewById(R.id.progress_bar_layout).setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.viewStarted();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.viewStopped();
    }
}
