package com.production.sergeypishiy.testtask.animals;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AnimalsApiClient {

    private static final String PLACES_API_URL = "http://kot3.com/";

    private static Retrofit placeClient = null;

    public static Retrofit getClient() {
        if(placeClient != null) {
            return placeClient;
        }
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(logger)
                .build();
        placeClient = new Retrofit.Builder()
                .baseUrl(PLACES_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return placeClient;
    }
}
