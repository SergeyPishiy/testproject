package com.production.sergeypishiy.testtask.animals;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AnimalsApiInterface {
    @GET("xim/api.php?")
    Call<AnimalsResponse> getAnimals(@Query(value = "query", encoded = true) String animal);
}
