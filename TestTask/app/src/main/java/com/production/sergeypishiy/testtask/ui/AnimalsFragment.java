package com.production.sergeypishiy.testtask.ui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.production.sergeypishiy.testtask.App;
import com.production.sergeypishiy.testtask.R;
import com.production.sergeypishiy.testtask.animals.AnimalsProvider;
import com.production.sergeypishiy.testtask.animals.AnimalsResponse;
import com.production.sergeypishiy.testtask.mvp.AnimalsFragmentPresenter;
import com.production.sergeypishiy.testtask.mvp.BaseAnimalsFragmentContract;
import com.production.sergeypishiy.testtask.mvp.MainActivityPresenter;

import javax.inject.Inject;

public class AnimalsFragment extends Fragment implements BaseAnimalsFragmentContract.View {

    public static final String ANIMAL_PROVIDER_KEY = "ANIMAL_PROVIDER";

    private AnimalsProvider animalsProvider;
    private RecyclerView rvAnimals;
    private AnimalAdapter animalAdapter;

    @Inject
    AnimalsFragmentPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_animals, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ((App)getActivity().getApplication()).getAppComponent().injectAnimalsFragment(this);

        rvAnimals = view.findViewById(R.id.rv_animal_list);
        animalsProvider = (AnimalsProvider) getArguments().getSerializable(ANIMAL_PROVIDER_KEY);

        animalAdapter = new AnimalAdapter(getActivity(), animalsProvider.getAnimals(), (position) -> {
            presenter.animalClicked(animalAdapter.getItem(position), position);
        });
        rvAnimals.setAdapter(animalAdapter);
        rvAnimals.setLayoutManager(new LinearLayoutManager(getActivity()));

        presenter.viewCreated(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void showAnimalDetails(AnimalsResponse.Animal animal, int position) {
        Intent intent = new Intent(getActivity(), AnimalDetailsActivity.class);
        intent.putExtra("animal", animal);
        intent.putExtra("number", position + 1);
        startActivity(intent);
    }
}