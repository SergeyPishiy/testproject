package com.production.sergeypishiy.testtask;

import android.app.Application;

import com.production.sergeypishiy.testtask.dagger.AppComponent;
import com.production.sergeypishiy.testtask.dagger.DaggerAppComponent;
import com.production.sergeypishiy.testtask.dagger.MainActivityPresenterModule;

public class App extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .mainActivityPresenterModule(new MainActivityPresenterModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
