package com.production.sergeypishiy.testtask.mvp;

import com.production.sergeypishiy.testtask.animals.AnimalsProvider;
import com.production.sergeypishiy.testtask.animals.AnimalsResponse;

public interface BaseAnimalsFragmentContract {

    interface View {
        void showAnimalDetails(AnimalsResponse.Animal animal, int position);
    }

    interface Presenter {
        void viewCreated(View mvpView);
        void animalClicked(AnimalsResponse.Animal animal, int position);
    }
}
